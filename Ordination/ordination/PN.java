package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> dageGivet = new ArrayList<>();
	private int antalGangeGivet = 0;

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	/**
	 * @param startDen
	 * @param slutDen
	 * @param laegemiddel
	 * @param antalEnheder
	 */
	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antalEnheder;

	}

	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.isBefore(super.getSlutDen()) && givesDen.isAfter(super.getStartDen())) {
			dageGivet.add(givesDen);
			antalGangeGivet++;
			return true;
		} else
			return false;
	}

	@Override
	public double doegnDosis() {

		if (dageGivet.size() == 0) {
			return 0;
		} else {

			LocalDate sidsteDato = dageGivet.get(dageGivet.size() - 1);

			int forskelDage = dageGivet.get(0).compareTo(sidsteDato);

			return (antalGangeGivet * getAntalEnheder()) / forskelDage;

		}
	}

	@Override
	public double samletDosis() {
		return antalGangeGivet * getAntalEnheder();
	}

	public int getAntalGangeGivet() {
		return antalGangeGivet;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

}
