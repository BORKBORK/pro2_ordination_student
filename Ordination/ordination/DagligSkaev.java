package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

	private ArrayList<Dosis> doser;
	private LocalTime tid;
	private ArrayList<LocalTime> klokkeSlet = new ArrayList<>();
	private double antal;

	/**
	 * @param startDen
	 * @param slutDen
	 * @param laegemiddel
	 * @param dosis
	 * @param tid
	 * @param antal
	 */

	public void opretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
	}

	/**
	 * @param startDen
	 * @param slutDen
	 * @param laegemiddel
	 * @param dosis
	 * @param tid
	 * @param antal
	 */
	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, ArrayList<Dosis> doser) {
		super(startDen, slutDen, laegemiddel);
		this.doser = doser;
	}

	@Override
	public double samletDosis() {
		double samlet = 0;
		for (Dosis obj : doser) {
			samlet += obj.getAntal();
		}
		return samlet;
	}

	@Override
	public double doegnDosis() {
		int antalDage = getSlutDen().compareTo(getStartDen());
		return samletDosis() / antalDage;
	}

	@Override
	public String getType() {
		return "Daglig skæv";
	}

	public ArrayList<Dosis> getDoser() {
		return doser;
	}

}
