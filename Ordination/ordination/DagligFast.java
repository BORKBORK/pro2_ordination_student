package ordination;

import java.time.*;
import java.time.temporal.ChronoUnit;

public class DagligFast extends Ordination {

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);

	}

	private Dosis[] doser = new Dosis[4];

	@Override
	public double samletDosis() {
		int antalDage = getSlutDen().compareTo(getStartDen());

		double samlet = 0;
		for (int j = 0; j < antalDage; j++) {
			for (int i = 0; i < doser.length; i++) {
				samlet += doser[i].getAntal();
			}

		}
		return samlet;
	}

	@Override
	public double doegnDosis() {
		int antalDage = getSlutDen().compareTo(getStartDen());
		return samletDosis() / antalDage;

		// TODO Virker ikke (tjek GUI)

	}

	@Override
	public String getType() {
		return "DagligFast";
	}

	public Dosis[] getDoser() {
		return doser;
	}

	public void setMorgenDosis(double antal) {

		Dosis dosisMorgen = new Dosis(LocalTime.of(6, 00), antal);
		doser[0] = dosisMorgen;
	}

	public void setMiddagDosis(double antal) {

		Dosis dosisMiddag = new Dosis(LocalTime.of(12, 00), antal);
		doser[1] = dosisMiddag;
	}

	public void setAftenDosis(double antal) {

		Dosis dosisAften = new Dosis(LocalTime.of(18, 00), antal);
		doser[2] = dosisAften;
	}

	public void setNatDosis(double antal) {

		Dosis dosisNat = new Dosis(LocalTime.of(00, 00), antal);
		doser[3] = dosisNat;
	}

}
