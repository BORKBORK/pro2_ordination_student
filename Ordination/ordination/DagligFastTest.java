package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import controller.Controller;
import storage.Storage;

public class DagligFastTest {
	private Patient patient;
	private Laegemiddel laegemiddel;
	private DagligFast dagligFast;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		// Controller.getController().opretPatient("1", "PLet", 20);
//		Controller.getController().opretLaegemiddel("AceSyre", 0.10, 0.15, 0.16, "Styk");
//		Controller.getController().opretDagligFastOrdination(LocalDate.of(2000, 01, 01), LocalDate.of(2000, 01, 10),
//				Controller.getController().getAllPatienter().get(0),
//				Controller.getController().getAllLaegemidler().get(0), 1, 1, 1, 1);

		Patient patient = new Patient("123", "TestMand", 80);
		Laegemiddel laegemiddel = new Laegemiddel("AceSyre", 0.10, 0.15, 0.16, "Styk");
		DagligFast dagligFast = new DagligFast(LocalDate.of(2000, 01, 01), LocalDate.of(2000, 01, 10), laegemiddel);

		dagligFast.setMorgenDosis(1);
		dagligFast.setMiddagDosis(1);
		dagligFast.setAftenDosis(1);
		dagligFast.setNatDosis(1);
		patient.addOrdination(dagligFast);
		System.out.println(dagligFast.samletDosis());
		this.dagligFast = dagligFast;
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSamletDosis() {

		double act = dagligFast.samletDosis();
		assertEquals(36.0, act, 0.00001);

	}

	@Test
	public void testDoegnDosis() {
		double act = dagligFast.doegnDosis();
		assertEquals(4.0, act, 0.0001);
	}

	@Test
	public void testDagligFast() {
		assertEquals(dagligFast, dagligFast);
		DagligFast dagligFast = new DagligFast(LocalDate.of(2000, 01, 01), LocalDate.of(2000, 01, 10), laegemiddel);
	}

}
